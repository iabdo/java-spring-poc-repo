package org.wwcode.springboot.GOTheroes.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.wwcode.springboot.GOTheroes.bo.GOTHero;
import org.wwcode.springboot.GOTheroes.service.GOTService;

@RestController
@RequestMapping(path="/GOT")
public class GOTHeroesController {
	
	@Value("${got.some-prop}")
	private String propertyValue;
	
	@Autowired
	private GOTService gotService;
	
	@RequestMapping(path="/heroes", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<GOTHero> heroes() {
		System.out.println("propertyValue = [" + propertyValue + "]");
		List<GOTHero> heroes = gotService.getAllHeroes();
		return heroes;
	}
	
	@RequestMapping(path="/hero/{id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GOTHero hero(@PathVariable int id, @RequestParam String param) {
		System.out.println("id = [" + id + "], param = [" + param + "]");
		return new GOTHero();
	}
}
