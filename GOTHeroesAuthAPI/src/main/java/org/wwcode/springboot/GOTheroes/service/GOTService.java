package org.wwcode.springboot.GOTheroes.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.wwcode.springboot.GOTheroes.bo.GOTHero;

@Service
public class GOTService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<GOTHero> getAllHeroes() {
		String sql = "select * from GOT_HEROES";
		List<GOTHero> heroes = new ArrayList<GOTHero>();
		jdbcTemplate.query(sql, (resultSet) -> {
			GOTHero hero = new GOTHero();
			hero.setId(resultSet.getInt(1));
			hero.setName(resultSet.getString(2));
			hero.setTitle(resultSet.getString(3));
			hero.setAllegiance(resultSet.getString(4));
			hero.setImageURL(resultSet.getString(5));
			heroes.add(hero);
		});
		return heroes;
	}
}
