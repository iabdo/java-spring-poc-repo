package org.wwcode.springboot.GOTheroes.bo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GOTHero {
	@JsonProperty(value = "hero-id")
	private int id;
	@JsonProperty(value = "hero-name")
	private String name;
	@JsonProperty(value = "hero-title")
	private String title;
	@JsonProperty(value = "hero-allegiance")
	private String allegiance;
	@JsonProperty(value = "hero-image-url")
	private String imageURL;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAllegiance() {
		return allegiance;
	}
	public void setAllegiance(String allegiance) {
		this.allegiance = allegiance;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
}
