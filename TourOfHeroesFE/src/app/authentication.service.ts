import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
    private authUrl = 'http://localhost:8080/auth';
    private headers = new HttpHeaders({'Content-Type': 'application/json'});
  constructor(private http: HttpClient, private messageService: MessageService) { }
     login(username: string, password: string): Observable<boolean> {
	    return this.http.post<any>(`${this.authUrl}`, JSON.stringify({username: username, password: password}), {headers: this.headers}).pipe(
	      map(data => {
	      	let result = data["token"]; 
	      	if(result != null && result != "") {
	      		localStorage.setItem('currentUser', JSON.stringify({ username: username, token: result }));
	      		this.log(`authenticated`);
	      		return true;
	      	} else {
	      		this.log(`not authenticated`)
	      		return false;
	      	}
	      }),
	      catchError(this.handleError<any>('login', []))
	    );
    }
    getToken(): String {
      var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      var token = currentUser && currentUser.token;
      console.log("token: " + token);
      return token ? token : "";
    }
    logout(): void {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    isLoggedIn(): Boolean {
    	var token: String = this.getToken();
  		return token && token.length > 0;
    }


   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
}