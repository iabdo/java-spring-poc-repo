package com.mazeed.tourofheroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.mazeed.tourofheroes")
public class TourOfHeroesBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TourOfHeroesBeApplication.class, args);
	}

}
