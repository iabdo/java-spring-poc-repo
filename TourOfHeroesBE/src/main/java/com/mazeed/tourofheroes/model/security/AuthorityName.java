package com.mazeed.tourofheroes.model.security;

public enum AuthorityName {
	ROLE_USER, ROLE_ADMIN
}
