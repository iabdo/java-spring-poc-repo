package com.vrtoonjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoicesIntegrationAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoicesIntegrationAppApplication.class, args);
	}

}
